'use strict';

const Renderer = function() {
	this.data = null
}

Renderer.prototype.render = function(data) {
	const self = this
	
	const newArray = new Uint8ClampedArray(self.data.data.length)
	let i = 0
	const amount = data.message.amount
	for (let y = 0; y < self.data.height; y++) {
		for (let x = 0; x < self.data.width; x++) {
			newArray[i] = self.data.data[i] * amount
			newArray[i+1] = self.data.data[i+1] * amount
			newArray[i+2] = self.data.data[i+2] * amount
			newArray[i+3] = self.data.data[i+3] // alpha
			i += 4
		}
	}

	postMessage({
		level: data.level,
		message: new ImageData(newArray, self.data.width, self.data.height),
		serial: data.serial,
	})
}

Renderer.prototype.setInitialData = function(imageData) {
	const self = this
	self.data = imageData
}

const r = new Renderer()

onmessage = function(e) {
	if (e.data.message['initialData']) {
		r.setInitialData(e.data.message['initialData'])
	} else if (e.data.message['amount']) {
		r.render(e.data)
	}
}

