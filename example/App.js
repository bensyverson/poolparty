'use strict';

var Point = function(x, y){
	this.x = x
	this.y = y
}

var App = function(){
	const self = this
	
	this.currentEl = document.getElementById('current')
	this.displayEl = document.getElementById('display')

	this.canvas = document.getElementById('canvas')
	this.context = this.canvas.getContext('2d')

	this.needsDisplay = false
	this.updatingDOM = false
	this.displayedSerial = 0
	this.displayedLevel = 0
	this.currentImageData = null
}

App.prototype.setup = function() {
	const self = this

	self.getImage()
	
	self.canvas.addEventListener('mousedown', function(e){
		e.preventDefault()
		const move = function(mvE) {
			mvE.preventDefault()
			const rect = self.canvas.getBoundingClientRect()
			const ratio = Math.max(Math.min((mvE.clientX - rect.left) / (rect.right - rect.left), 1.0), 0.0)
			self.renderer.renderUpdate({'amount': ratio * 2.0})
		}

		const up = function(upE) {
			self.canvas.removeEventListener('mousemove', move)
			self.canvas.removeEventListener('mouseup', up)
		}
		self.canvas.addEventListener('mousemove', move)
		self.canvas.addEventListener('mouseup', up)
	})

	self.requestFrame()
}


App.prototype.getImage = function(fidelity) {
	const self = this
	const image = new Image()
	image.onload = function() {
		self.setInitialData(image)
	}
	image.src = 'beijo.jpg';
}

App.prototype.setInitialData = function(image) {
	const self = this

	self.renderer = PoolParty.InteractiveRenderer.withOptions({
		workerScript: 'worker.js',
		renderLevels: 3,
		initialData: [
			self.resizedData(image, 0.25),
			self.resizedData(image, 0.5),
			self.resizedData(image, 1.0)
		],
		callback: function renderDone(message){
			self.displayedSerial = message.serial
			self.displayedLevel = message.level
			self.currentImageData = message.message
			self.setNeedsDisplay()
		}
	})

	self.renderer.renderUpdate({amount: 1.0})
}

App.prototype.resizedData = function(image, scale) {
	const self = this
	const canvas = document.createElement('canvas')
	const ctx = canvas.getContext('2d')
	const width = image.width * scale
	const height = image.height * scale
	canvas.width = width
	canvas.height = height
	ctx.drawImage(image, 0, 0, width, height)
	return ctx.getImageData(0, 0, width, height)
}

App.prototype.setNeedsDisplay = function() {
	const self = this
	if (!self.updatingDOM) {
		self.needsDisplay = true
	}
}

App.prototype.requestFrame = function() {
	const self = this
	window.requestAnimationFrame(function callRenderLoop(timestamp){
		self.renderDOM()
	})
}

App.prototype.renderDOM = function() {
	const self = this
	if (!self.needsDisplay) { return self.requestFrame() }
	self.updatingDOM = true
	
	// Update serials and draw in canvas
	self.currentEl.innerHTML = self.renderer.dispatchedSerial
	self.displayEl.innerHTML = self.displayedSerial + " (" + self.displayedLevel + ")"

	self.canvas.width = self.currentImageData.width
	self.canvas.height = self.currentImageData.height
	self.context.putImageData(self.currentImageData, 0, 0)
	
	// reset flags
	self.needsDisplay = false
	self.updatingDOM = false

	self.requestFrame()
}