const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
	entry: './src/InteractiveRenderer.ts',
	mode: 'production',
	output: {
		library: 'PoolParty',
		libraryTarget: 'var'
	},
	// devtool: 'inline-source-map',
	devtool: 'sourcemap',
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			}
		]
	},
	resolve: {
		extensions: [".ts", ".tsx", ".js"]
	},
	optimization: {
		minimizer: [
			new TerserPlugin({
				terserOptions: {
					ecma: undefined,
					warnings: false,
					parse: {},
					compress: {},
					mangle: {
						properties: {
							keep_quoted: true,
							reserved: []
						}
					},
					module: false,
					output: null,
					toplevel: false,
					nameCache: null,
					ie8: false,
					keep_classnames: false,
					keep_fnames: false,
					safari10: true
				}
			})
		]
	}
}