export class WorkerMessage {
	public serial: number
	public message: object
	public level: number
	constructor(aSerial: number, aLevel: number, aMessage: object) {
		this['serial'] = aSerial
		this['message'] = aMessage
		this['level'] = aLevel
	}
}

class WorkerWrapper {
	public isWorking = false
	public isTerminated = false
	public startTime = 0
	public speed = 0
	public worker: Worker
	constructor(private script: string){
		this.worker = new Worker(script)
	}
}

export class WorkerPool {
	private workers: WorkerWrapper[] = []
	private internalCount = 0
	private speeds = new Uint32Array(100)
	private lastSpeed = 0
	private currentSerial = 0
	public completedSerial = 0

	constructor(private script: string,
		public fidelityLevel: number,
		private initialData: object,
		private callback: (message: WorkerMessage) => void) {
		this.addWorker()
	}

	get averageSpeed(): number {
		const count = Math.min(this.internalCount, this.speeds.length)
		if (count < 2) { return this.lastSpeed }
		let x = 0
		for (let i = 0; i < count; i++){
			x += this.speeds[i]
		}
		return x / (count - 1)
	}

	get isRendering(): boolean {
		return this.workers
			.map(worker => worker.isWorking)
			.reduce((acc, isWorking) => isWorking || acc, false)
	}

	get isInteractive(): boolean { return this.averageSpeed < 20 }

	addWorker() {
		const self = this
		const worker = new WorkerWrapper(self.script)
		worker.worker.onmessage = (e) => {
			worker.isWorking = false
			worker.speed = performance.now() - worker.startTime
			self.lastSpeed = worker.speed
			self.speeds[self.internalCount % 100] = worker.speed
			self.internalCount += 1
			const aSerial = e.data['serial']
			if (aSerial < self.currentSerial) { return }
			if (aSerial > self.completedSerial) { self.completedSerial = aSerial}
			self.callback(new WorkerMessage(self.currentSerial, self.fidelityLevel, e.data.message))
		}
		worker.worker.postMessage(new WorkerMessage(0, self.fidelityLevel, {
			'initialData': self.initialData
		}))
		self.workers.push(worker)
		return worker
	}

	sendUpdate(msg: WorkerMessage) {
		if (msg.serial < this.currentSerial) { return }
		let worker = this.workers.filter(worker => !worker.isWorking)[0]
		if (!worker) { worker = this.addWorker() }
		worker.startTime = performance.now()
		worker.isWorking = true
		this.currentSerial = msg.serial
		worker.worker.postMessage(msg)
	}

	terminateWorkingThreads(){
		const self = this
		const working = self.workers.filter(worker => worker.isWorking)
		const skip = (working.length == self.workers.length) ? 0 : -1
		self.workers = self.workers
			.map((worker, idx) => {
				if (worker.isWorking && (idx != skip)) {
					worker.worker.terminate()
					worker.isTerminated = true
				}
				return worker
			}).filter(worker => !worker.isTerminated)
	}

	terminateAllThreads(){
		this.workers.forEach(worker => worker.worker.terminate() )
		this.workers = []
	}
}