import { WorkerPool, WorkerMessage } from './WorkerPool'

export class InteractiveRenderer {
	private currentSerial = 0
	private displayedSerial = 0
	private displayedLevel = 0
	private interactiveLevel = 0
	private pools: WorkerPool[] = []
	private timer = 0
	private update: object = {}

	constructor(public readonly script: string, 
		public readonly levelCount: number,
		initialData: [object],
		private completion: (message: WorkerMessage) => void) {
		const workerUpdate = (message: WorkerMessage) => this.renderDone(message)
		for (var i=0; i < levelCount; i++) {
			this.pools.push(new WorkerPool(script, i, initialData[Math.min(i, initialData.length)], workerUpdate))
		}
	}

	static 'withOptions'(options: {workerScript: string, renderLevels: number, initialData: [object], callback: (message: WorkerMessage) => void}) {
		return new InteractiveRenderer(options['workerScript'], options['renderLevels'], options['initialData'], options['callback'])
	}

	get 'dispatchedSerial'(): number { return this.currentSerial }

	'renderUpdate'(anUpdate: object) {
		this.currentSerial++; this.dispatchRender(anUpdate)
	}

	dispatchRender(anUpdate: object) {
		this.update = anUpdate
		const pool = this.pools[this.interactiveLevel]
		if (pool.isRendering) { return }
		pool.sendUpdate(
			new WorkerMessage(this.currentSerial, pool.fidelityLevel, anUpdate)
		)
	}

	highestInteractiveLevel(){
		return this.pools
			.slice()
			.reverse()
			.filter(pool => pool.isInteractive)
			.map(pool => pool.fidelityLevel)[0] || 0
	}

	renderDone(message: WorkerMessage) {
		const self = this
		const pool = self.pools[message.level]
		if ((message.serial > self.displayedSerial) ||
			(message.serial == self.displayedSerial) && (message.level > self.displayedLevel)) {
			self.displayedSerial = message.serial
			self.displayedLevel = message.level
			self.completion(message)
		}

		if (pool.isInteractive) {
			if (pool.fidelityLevel > self.interactiveLevel) {
				self.interactiveLevel = self.highestInteractiveLevel()
			}
		} else {
			if ((pool.fidelityLevel > 0) && (pool.fidelityLevel == self.interactiveLevel)) {
				self.interactiveLevel = self.highestInteractiveLevel()
			}
		}
		// Terminate sub-interactive threads
		this.pools
			.slice(0, this.interactiveLevel)
			.forEach(pool => pool.terminateAllThreads())

		if (self.currentSerial > pool.completedSerial) {
			// If the display is out of date, start a fresh render (it may have already happened, in which case it will be ignored.)
			self.dispatchRender(self.update)
		} else {
			if (self.timer) { clearTimeout(self.timer) }
			if (pool.fidelityLevel == self.interactiveLevel) {
				this.pools
					.slice(this.interactiveLevel)
					.forEach(pool => pool.terminateWorkingThreads())

				let serial = self.currentSerial
				self.timer = setTimeout(()=>{
					self.levelUp(pool.fidelityLevel, serial)
				}, 25)
			}
		}
	}

	levelUp(from: number, serial: number) {
		if (serial < this.currentSerial) { return }
		this.pools.slice(from).forEach(pool =>
			pool.sendUpdate(
				new WorkerMessage(this.currentSerial, pool.fidelityLevel, this.update)
			)
		)
	}
}